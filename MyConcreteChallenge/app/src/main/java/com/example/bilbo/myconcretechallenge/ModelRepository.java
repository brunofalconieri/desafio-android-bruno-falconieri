package com.example.bilbo.myconcretechallenge;

import org.json.JSONObject;

/**
 * Created by Bilbo on 17/02/2018.
 */

public class ModelRepository {
    private String name;
    private String description;
    private String login;
    private String avatar_url;
    private String stargazers_count;
    private String forks_count;



    public ModelRepository(String name, String description,String login, String avatar_url, String stargazers_count, String forks_count){
        this.name = name;
        this.description = description;
        this.login = login;
        this.avatar_url = avatar_url;
        this.stargazers_count = stargazers_count;
        this.forks_count = forks_count;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(String stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public String getForks_count() {
        return forks_count;
    }

    public void setForks_count(String forks_count) {
        this.forks_count = forks_count;
    }
}
