package com.example.bilbo.myconcretechallenge;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bilbo on 17/02/2018.
 */

public class PullRequests extends AppCompatActivity {


    AdapterPulls adapterPulls;
    RecyclerView rvpull;
    List<ModelPulls> mmodelpulls;
    RecyclerView.LayoutManager mpulllayout;
    String login_segment_url, namerepository_segment_url;
    String urlpull_partial ="https://api.github.com/repos/";
    String urlpull_full;

    RequestQueue mRequestQueue;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pull_request);




        rvpull = (RecyclerView) findViewById(R.id.recycler_pull);
        mpulllayout = new LinearLayoutManager(this);
        rvpull.setLayoutManager(mpulllayout);
        mmodelpulls = new ArrayList<>();

        adapterPulls = new AdapterPulls(PullRequests.this, mmodelpulls);
        rvpull.setAdapter(adapterPulls);

        login_segment_url = getIntent().getStringExtra("key_login");
        namerepository_segment_url = getIntent().getStringExtra("key_reponame");

        urlpull_full = urlpull_partial.concat(login_segment_url).concat("/")
                .concat(namerepository_segment_url).concat("/pulls");

         setTitle(namerepository_segment_url);

        mRequestQueue = Volley.newRequestQueue(this);


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET,urlpull_full, null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {

                       try {



                            for (int i = 0; i < response.length(); i++) {

                                JSONObject pullroot = response.getJSONObject(i);
                                JSONObject child = pullroot.getJSONObject("user");


                                ModelPulls modelPullsitem = new ModelPulls(child.getString("login"),child.getString("avatar_url"),
                                       pullroot.getString("title"),pullroot.getString("created_at"), pullroot.getString("body"),
                                        pullroot.getString("html_url"));

                                mmodelpulls.add(modelPullsitem);

                            }

                            adapterPulls.notifyDataSetChanged();

                       } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mRequestQueue.add(jsonArrayRequest);


        adapterPulls.setOnItemClickListener(new AdapterPulls.ClickListenerPull() {
            @Override
            public void onItemClick(int position, View v) {

                ModelPulls modelPullsurl = mmodelpulls.get(position);
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(modelPullsurl.getHtml_url()));
                startActivity(intent);
            }
        });

}

}
