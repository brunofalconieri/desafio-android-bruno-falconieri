package com.example.bilbo.myconcretechallenge;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import android.widget.ImageButton;

import android.widget.TextView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    AdapterRecycler mAdapter;
    RecyclerView rv;
    List<ModelRepository> listmodel;
    RecyclerView.LayoutManager mlayout;
    RequestQueue mRequestQueue;

    String partial_repo_url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=";

    String full_repo_url;
    int page = 1;
    String cpage = "1";
    TextView currentpage;
    ImageButton minospage, pluspage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv = (RecyclerView) findViewById(R.id.recycler_main);
        currentpage = (TextView) findViewById(R.id.current_page);
        minospage = (ImageButton) findViewById(R.id.minos_page);
        pluspage = (ImageButton) findViewById(R.id.plus_page);

        mlayout = new LinearLayoutManager(this);
        rv.setLayoutManager(mlayout);
        listmodel = new ArrayList<>();

        mAdapter = new AdapterRecycler(MainActivity.this, listmodel);
        rv.setAdapter(mAdapter);

        mRequestQueue = Volley.newRequestQueue(this);


        pluspage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                minospage.setEnabled(true);
                page = page + 1;
                cpage = String.valueOf(page);
                currentpage.setText(cpage);
                full_repo_url = partial_repo_url.concat(cpage);
                connect();


            }
        });

        minospage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                page = page - 1;

                if(page<1) {

                    minospage.setEnabled(false);
                    page = 1;


                }

                else {

                    cpage = String.valueOf(page);
                    currentpage.setText(cpage);
                    full_repo_url = partial_repo_url.concat(cpage);
                    connect();
                }
               }

        });

         minospage.setEnabled(false);
         currentpage.setText(cpage);
         full_repo_url = partial_repo_url.concat(cpage);
         connect();

        mAdapter.setOnItemClickListener(new AdapterRecycler.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {



                ModelRepository modelRepos = listmodel.get(position);
                Intent intent = new Intent(v.getContext(), PullRequests.class);

                intent.putExtra("key_login", modelRepos.getLogin());
                intent.putExtra("key_reponame", modelRepos.getName());
                startActivity(intent);

            }
        });
    }

    private void connect () {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, full_repo_url, null,
                new Response.Listener<JSONObject>() {


                    @Override

                    public void onResponse(JSONObject response) {

                        try {

                            listmodel.removeAll(listmodel);

                            JSONArray jsonArray = response.getJSONArray("items");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject object = jsonArray.getJSONObject(i);
                                JSONObject child = object.getJSONObject("owner");


                                ModelRepository modelRepository = new ModelRepository(object.getString("name"), object.getString("description"),
                                        child.getString("login"), child.getString("avatar_url"), object.getString("stargazers_count"), object.getString("forks_count"));

                                listmodel.add(modelRepository);

                            }

                            mAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mRequestQueue.add(jsonObjectRequest);

    }


}

