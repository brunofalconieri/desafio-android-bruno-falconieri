package com.example.bilbo.myconcretechallenge;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Bilbo on 17/02/2018.
 */

public class AdapterRecycler extends RecyclerView.Adapter<AdapterRecycler.ViewHolder>{

    List<ModelRepository> modelRepositories;
    Context context;
    private static ClickListener clickListener;

    public AdapterRecycler(Context context, List<ModelRepository> modelRepositories){
        this.modelRepositories = modelRepositories;
        this.context = context;
    }

    @Override
    public AdapterRecycler.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.list_respository,parent,false);
        //ViewHolder holder = new ViewHolder(v);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdapterRecycler.ViewHolder holder, int position) {

        ModelRepository modelRepository = modelRepositories.get(position);
        holder.name_user.setText(modelRepository.getLogin());
        holder.name_repos.setText(modelRepository.getName());
        holder.descript_repos.setText(modelRepository.getDescription());
        holder.star.setText(modelRepository.getStargazers_count());
        holder.fork.setText(modelRepository.getForks_count());

        Glide.with(holder.img_user.getContext())
                .load(modelRepository.getAvatar_url())
                .into(holder.img_user);



    }

    @Override
    public int getItemCount() {
        return modelRepositories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView name_user,name_repos,descript_repos,star,fork;
        ImageView img_user;

        public ViewHolder(View itemView) {
            super(itemView);


            name_user = (TextView)itemView.findViewById(R.id.user_name);
            name_repos = (TextView)itemView.findViewById(R.id.respository_name);
            descript_repos = (TextView)itemView.findViewById(R.id.respository_descript);
            img_user = (ImageView) itemView.findViewById(R.id.user_image);
            star = (TextView)itemView.findViewById(R.id.number_stars);
            fork = (TextView)itemView.findViewById(R.id.number_forks);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(),v);

        }
    }


    public void setOnItemClickListener(ClickListener clickListener) {
        AdapterRecycler.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);

    }
}


