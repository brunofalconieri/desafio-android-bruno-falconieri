package com.example.bilbo.myconcretechallenge;

/**
 * Created by Bilbo on 17/02/2018.
 */

public class ModelPulls {

    private String login;
    private String avatar_url;
    private String title;
    private String created_at;
    private String body;
    private String html_url;


    public ModelPulls(String login, String avatar_url, String title, String created_at, String body, String html_url) {
        this.login = login;
        this.avatar_url = avatar_url;
        this.title = title;
        this.created_at = created_at;
        this.body = body;
        this.html_url = html_url;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getBody() {
        return body;
    }

     void setBody(String body) {
        this.body = body;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String url) {
        this.html_url = html_url;
    }
}



