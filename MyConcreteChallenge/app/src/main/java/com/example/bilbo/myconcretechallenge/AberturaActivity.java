package com.example.bilbo.myconcretechallenge;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class AberturaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abertura);

        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                telaAbertura();
            }
        },3500);

    }

    private void telaAbertura(){

        Intent intent = new Intent(AberturaActivity.this,
                MainActivity.class);
        startActivity(intent);
        finish();
    }
}
