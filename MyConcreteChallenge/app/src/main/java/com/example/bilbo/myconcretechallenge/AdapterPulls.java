package com.example.bilbo.myconcretechallenge;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Bilbo on 17/02/2018.
 */

public class AdapterPulls extends RecyclerView.Adapter<AdapterPulls.ViewHolder> {


    List<ModelPulls> modelPulls;
    Context context;
    private static ClickListenerPull clickListenerpull;

    public AdapterPulls(Context context, List<ModelPulls> modelPulls){
        this.modelPulls = modelPulls;
        this.context = context;
    }


    @Override
    public AdapterPulls.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.list_pulls,parent,false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ModelPulls model_pulls = modelPulls.get(position);

        String date;
        String time;
        String finaltime;
        String[] datetime = model_pulls.getCreated_at().split("T");
        date = datetime[0];
        time = datetime[1];
        finaltime = time.substring(0,8);
        holder.login_pull.setText(model_pulls.getLogin());
        holder.title_pull.setText(model_pulls.getTitle());
        holder.body_pull.setText(model_pulls.getBody());
        holder.date_pull.setText(date);
        holder.time_pull.setText(finaltime);

        Glide.with(holder.img_userpull.getContext())
                .load(model_pulls.getAvatar_url())
                .into(holder.img_userpull);


    }



    @Override
    public int getItemCount() {
        return modelPulls.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView login_pull,title_pull,body_pull,date_pull,time_pull;
        ImageView img_userpull;

        public ViewHolder(View itemView) {
            super(itemView);

            login_pull = (TextView)itemView.findViewById(R.id.pulluser_name);
            title_pull = (TextView)itemView.findViewById(R.id.tittlepull_name);
            body_pull = (TextView)itemView.findViewById(R.id.pull_body);
            date_pull = (TextView)itemView.findViewById(R.id.pull_date);
            img_userpull = (ImageView)itemView.findViewById(R.id.pulluser_image);
            time_pull = (TextView)itemView.findViewById(R.id.pull_time);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
           clickListenerpull.onItemClick(getAdapterPosition(),v);
        }
    }

    public void setOnItemClickListener(ClickListenerPull clickListenerpull) {
        AdapterPulls.clickListenerpull = clickListenerpull;
    }

    public interface ClickListenerPull {
        void onItemClick(int position, View v);

    }

}
